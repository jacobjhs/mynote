# Sweety Marry
在西方，结婚六年年常叫做铁婚，按照中国的传统，一般叫做糖婚，或者锡婚，我是比较喜欢糖婚这个名字，寓意很好。
最近两年工作很忙，一半是应急处置，一半也干着与文字打交道的工作，博客里也写了一些与技术、工作相关的文章，但却少了家庭生活的记录，记得孩子出生的那几年，每年都写一些记录孩子出生后的家庭生活，感想颇多，为了两个宝宝的成长、生活的纷扰，时常是报团取暖，满心期待着那种没有睡眠的日子赶快过去。随着孩子们的成长渐渐相濡以沫，少了争吵，多了包容。多了些人生的积淀，少了对你的嘘寒问暖。
家庭总是一个男人卸下疲惫的港湾，每天晚上家务做完后的11点，洗个澡聊会天或者看会手机就是漫漫生活的一天，只有周末偶尔能有个一天的假可以陪陪你和孩子。每个家庭都有各自的纷扰，我们获得的，就是把有限的时间一起陪着孩子成长，承受的，是周而复始的家务与工作。相比结婚的前几年，你改变了很多，矫情少了，耐性多了，婚姻生活让人成熟啊，不仅是成熟的鱼尾纹、法令纹，还有消失殆尽的妊娠纹。
希望我们并肩携手，朝着皱纹，越过山海，享受安宁……