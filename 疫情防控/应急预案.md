1. 【腾讯文档】九华山风景区新冠肺炎疫情防控工作方案--流调组工作方案
https://docs.qq.com/doc/DZVlvQklLQ1RHZWVj

2. 【腾讯文档】九华山风景区疫情防控社会维稳现场处置专班人员名单
https://docs.qq.com/sheet/DZXVCWFJUc1pGTk9H

3. 【腾讯文档】九华山风景区疫情防控社会维稳综合协调专班人员名单
https://docs.qq.com/sheet/DZWxIbENZdGN1ZWdH

4. 【腾讯文档】九华山风景区疫情防控游客安置专班人员名单
https://docs.qq.com/sheet/DZWh1RHZ1THBYdE5D

5. 【腾讯文档】九华山风景区疫情防控游客服务专班人员名单
https://docs.qq.com/sheet/DZVVPa3ZZeW9kdEVq