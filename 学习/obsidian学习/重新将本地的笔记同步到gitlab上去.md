# 重新将本地的笔记同步到gitlab上去
###  因为GitHub默认的账户密码找不回来，就试了试看看gitlab怎么样
还行，不算复杂。
____________________
1. 目前我的多客户端同步主要依靠的是Resilio Sync这个软件，主要就是各平台都有客户端，包括Linux的Openwrt系统都有 ，可以有效保护数据不会丢失，但git到gitlab等于再加了一个仓库。resilio在手机上的同步有点小问题，作为笔记的同步工具略有不足
2. 参考了这篇文章[少数派](https://sspai.com/post/68989) 新建一个bash脚本
代码如下
```bash
#!/bin/bash 
splitLine="=====================================================" 
userName="这里改成你的用户名" 
email="这里改成你的Email"

# 配置 git 用户名和邮箱 
git config --global user.name ${userName} 
git config --global user.email ${email} 
git config --global --list 
echo "Enter 或者 y 键确认" 
ssh-keygen -t rsa -C "${email}" 

# 打印公钥 
echo "复制保存下面的公钥添加到远程仓库" 
echo ${splitLine} 
cat ~/.ssh/id_rsa.pub 
echo ${splitLine} 
echo "按任意键退出" 
read -n 1 
echo "继续运行"
```
3. 执行脚本，生成公钥上传到gitlab上!
![[Pasted image 20220125124207.png]]

4. 进入笔记所在文件夹，执行
```bash
git push -u -f origin master
```

![[Pasted image 20220125124406.png]]
如果发生此类报错，
```bash
To gitlab.com:jacobjhs/mynote.git
 ! [remote rejected] master -> master (pre-receive hook declined)
error: failed to push some refs to 'gitlab.com:jacobjhs/mynote.git'
```
请在下图中，将Protected Branches里将![[Pasted image 20220125124636.png]]![[Pasted image 20220125124806.png]]关闭
再次重新运行
```bash
git push -u -f origin master
```

**Tips** 生成公钥的时候不要设置密码，否则导致obsidian git插件不能自动上传