# 一级标题
## 二级标题
### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题
 由此可见obsidian的只支持三级以内标题，四五六级标题同三级标题
 
 ### 字体
 *斜体文本*
 _xieti wentben_
 
 **粗体文本**
 ***粗斜体文本***
 
 ### 分割线
 ***
 * * *
 ******
 - - -
 ----------
 分割线不区分样式
 
 ### 删除线
 ~~google~~   ~~~~
 
 ### 下划线
 <u>下划线</u>
	
### 列表

* one 
* two
* three

1. one
2. two
3. three

#### 列表嵌套
1. one
	- one
	- two
2. one
	- one
	- two

## 区块
> one
> > two
> > > three

### 区块中使用列表
> 区块列表
> 1. one
> 2. two
>  + first
>  + second

### 列表中使用区块
* one
	> first
	> second
* two

## 代码 coding
``` python
 def function():
 	balabala
```

## 链接
[链接名称](http://baidu.com)
[[coding  https://google.com]]

<https://google.com>
[google]: https://google.com

![]()

## 表格
| 左对齐 | 右对齐 | 居中对齐 |
| :---- | ----: | :----: |
| 单元格 | 单元格|单元格|
\*
\[]
\{}

$$
\mathbf{V}_1 \times \mathbf{V}_2 = \begin{vmatrix} \mathbf{i} & \mathbf{j} & \mathbf{k} \\
\frac{\partial X}{\partial u} & \frac{\partial Y}{\partial u} & 0 \\
\frac{\partial X}{\partial v} & \frac{\partial Y}{\partial v} & 0 \\
\end{vmatrix} ${$tep1}{\style{visibility:hidden}{(x+1)(x+1)}} $$




